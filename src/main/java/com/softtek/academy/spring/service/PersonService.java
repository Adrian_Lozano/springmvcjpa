package com.softtek.academy.spring.service;

import java.util.List;

import com.softtek.academy.spring.beans.Person;

public interface PersonService {
	List<Person> getAll();
	Person getById(int id);
	void update(Person p);
	void delete(int id);
	void savePerson(Person person);
}
