package com.softtek.academy.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService{
	public PersonServiceImpl() {
	}
	@Autowired
	@Qualifier("personRepository")
	private PersonRepository personRepository;
	@Override
	public List<Person> getAll() {
		return personRepository.getAll();
	}

	public PersonRepository getPersonRepository() {
		return personRepository;
	}

	public void setPersonRepository(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@Override
	public Person getById(int id) {
		return personRepository.getPersonById(id);
	}

	@Override
	public void update(Person p) {
		personRepository.update(p);
	}

	@Override
	public void delete(int id) {
		personRepository.delete(id);
		
	}


	@Override
	public void savePerson(Person person) {
		personRepository.savePerson(person);
	}

}
